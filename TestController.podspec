Pod::Spec.new do |s|
  s.name             = 'TestController'
  s.version          = '1.0'
  s.summary          = 'By far the most fantastic view I have seen in my entire life. No joke.'
 
  s.description      = <<-DESC
This fantastic view changes its color gradually makes your app look fantastic!
                       DESC
 
  s.homepage         = 'https://bitbucket.com/srini2hash/TestController'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Srini" => "srini2dl@gmail.com" }
  s.source           = { :git => 'https://bitbucket.com/srini2hash/TestController.git', :tag => 1.0 }
 
  s.ios.deployment_target = '12.1'
  s.source_files = 'TestController/TestViewController.swift'
 
end